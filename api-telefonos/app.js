const config = require(`./config`)
const express = require('express');
const funciones = require(`./funciones`);





const app = express();

app.get('/', function (req, res) {
  res.send();
});

app.get('/primeramitadlista', function (req, res) {
  res.send(funciones.primeraMitadCelulares());
});

app.get('/segundamitadlista', function (req, res) {
  res.send(funciones.segundaMitadCelulares());
});

app.get('/menorprecio', function (req, res) {
  res.send(funciones.menorPrecioCelulares());
});

app.get('/mayorprecio', function (req, res) {
  res.send(funciones.mayorPrecioCelulares());
});

app.get('/listadegamas', function (req, res) {
  res.send(funciones.gamaCelulares());
});



app.listen(config.port, function () {
  console.log(`Escuchando el puerto: ${config.port}`);
});
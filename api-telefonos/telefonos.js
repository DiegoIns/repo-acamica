const telefonos = [
    {
        marca: `Samsung`,
        gama:`Media`, 
        modelo:`Galaxy A51`,
        pantalla:`6,5" pulgadas Super AMOLED`,
        so:`Android 10`,
        precio:219999,
    },
    {
        marca: `Nokia`,
        gama:`Alta`, 
        modelo:`Galaxy A71`,
        pantalla:`OLED de 6" FHD+, 21:9 Gorilla Glass 6`,
        so:`Android 10`,
        precio:300000,
        
    },
    {
        marca: `Alcatel`,
        gama:`Baja`, 
        modelo:`Y600D`,
        pantalla:`OLED de 7"`,
        so:`Android 10`,
        precio:400000,
    },
    {
        marca: `Huawei`,
        gama:`Media`, 
        modelo:`P8`,
        pantalla:`OLED de 6"`,
        so:`Android 10`,
        precio:500000,
    },
]





module.exports = telefonos
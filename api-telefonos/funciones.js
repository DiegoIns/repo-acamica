const telefonos = require(`./telefonos`);

function menorPrecioCelulares() {
    telefonos.sort((a,b) => a.precio - b.precio);
    return telefonos[0];
}
function mayorPrecioCelulares() {
    telefonos.sort((a,b) => a.precio - b.precio);
    return telefonos[telefonos.length - 1];
}


function primeraMitadCelulares(params) {
    let primeraMitad = telefonos.slice(0,(telefonos.length/2));
    
    return primeraMitad;   
}
function segundaMitadCelulares(params) {
    let segundaMitad = telefonos.slice(telefonos.length/2); 

    return segundaMitad;
}


function gamaCelulares() {
    let gamaAlta = [],
        gamaMedia = [],
        gamaBaja = [];
    for (let i = 0; i < telefonos.length; i++) {
        switch (telefonos[i].gama) {
            case 'Alta':
                gamaAlta.push(telefonos[i]);
                break;
            case 'Media':
                gamaMedia.push(telefonos[i]);
                break;
            case 'Baja':
                gamaBaja.push(telefonos[i]);
                break;
            default:
                break;
        }
    }
    return gamas = [gamaBaja,gamaMedia,gamaAlta];
}




module.exports = {menorPrecioCelulares,mayorPrecioCelulares,primeraMitadCelulares,segundaMitadCelulares,gamaCelulares}
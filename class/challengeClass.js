class Estudiante {
    constructor(nombre, apellido, edad){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;

    }
    fullname(){
        console.log(`mi nombre es ${this.nombre} ${this.apellido} `);
    }
    es_mayor(){
        console.log(this.edad >=18);
    }
}

let Diego = new Estudiante ("Diego", "Insaurralde" , 30)
let Maru = new Estudiante ("Maria", "Paredes", 31)

Diego.fullname();
Diego.es_mayor();
Maru.fullname();
Maru.es_mayor();
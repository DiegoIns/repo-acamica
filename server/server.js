const config = require(`./config`);
const express = require('express');
const marcasAutos = require(`./data`);


const app = express();



app.get('/', function (req, res) {
  res.send(marcasAutos);
});

app.get('/autos', function (req, res) {
  const auto = marcasAutos[0];
    res.send(auto);
});

app.post(`/autos`, (req, res) => {
    res.status(201).send(`Ruta para crear un Auto`);
})

app.listen(config.port, function () {
  console.log(`Escuchando el puerto en http://localhost:${config.port}`);
});
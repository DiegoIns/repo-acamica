const marcasAutos = [
    {
        id : 1,
        nombre: `Fiat`,
        modelo: `Uno`,
    },
    {
        id : 2,
        nombre: `Volkswagen`,
        modelo: `Golf`,
    }
]

module.exports = marcasAutos
const config = require('./config');
const express = require('express');
const arrayAutores = require("./autores");
const funciones = require("./funciones");

const app = express()







//devuelve todos los autores
app.get(`/autores`, (req, res) =>
{
    res.json(arrayAutores)
})

//crea un auto nuevo
app.post('/autores', (req, res) => {

    res.status(201).json('Ruta para crear un nuevo autor')
})

//devuelve autor con ID indicado
app.get(`/autores/:id`, funciones.validarIdAutor,(req, res) => {
    const idAutor = parseInt(req.params.id);
    const autor =arrayAutores.find(autor => autor.id === idAutor)

    if (!autor) {
        res.status(400).json(`No se encuentra el autor con el id ${idAutor}`);
    }

})

app.delete('/autores/:id',funciones.validarIdAutor, (req, res) => {

    // obtenermos el id de la ruta
    const idAutor = parseInt(req.params.id);
    
    // buscamos si el auto existe
    const autor = arrayAutores.find(autor => autor.id === idAutor);
    // si no existe damos un error
    if (!autor) {
        res.status(400).json(`No se encuentra el autor con id ${idAutor}`)
        return
    }

    // elimiar el auto
    const posAutorId = arrayAutores.indexOf(autor)

    arrayAutores.splice(posAutorId, 1)
    
    res.json(arrayAutores)
})

app.put(`/autores/:id`, funciones.validarIdAutor,(req, res) => {
    const idAutor = parseInt(req.params.id);
    const autor =arrayAutores.find(autor => autor.id === idAutor)

    if (!autor) {
        res.status(400).json(`No se encuentra el autor con el id ${idAutor}`);
    }

})


///////// LIBROS ////////////


app.get(`/autores/:id/libros`, funciones.validarIdAutor,(req, res) => {
    const idAutor = parseInt(req.params.id);
    const autor = arrayAutores.find(autor => autor.id === idAutor)

    if (!autor) {
        res.status(400).json(`No se encuentran los libros del autor con el id ${idAutor}`);
    }
    
    
    //obtiene el índice del autor en el array
    const indexAutor = arrayAutores.indexOf(autor);

    //respuesta con los libros del autor
    res.status(200).json(arrayAutores[indexAutor].libros);

})


app.post(`/autores/:id/libros`, funciones.validarIdAutor,(req, res) => {
    const idAutor = parseInt(req.params.id);
    const autor = arrayAutores.find(autor => autor.id === idAutor)

    res.status(201).json(`se ha agregado un libro al autor con id ${idAutor}`);

})


app.get('/autores/:id/libros/:idLibro', funciones.validarIdLibro, (req, res) => {
    //obtiene el id de la ruta
    const idAutor = parseInt(req.params.id);
    const autor = arrayAutores.find(autor => autor.id === idAutor);

    //valida si el autor existe
    if (!autor) {
        res.status(400).json(`No se encuentran el autor con el id ${idAutor}`);
    }

    //obtiene el id del libro de la ruta
    const idLibro = parseInt(req.params.idLibro);
    const libro = autor.libros.find(libro => libro.id === idLibro);

    //valida la existencia del libro dentro del autor
    if (!libro) {
        res.status(400).json(`No se encuentran el libro con el id ${idLibro}`);
    }

    res.status(200).json(libro);

})














app.listen(config.port, () => {
    console.log(`Example app listening at http://localhost:${config.port}`)
  })
const arrayAutores = [
    {
        id : 1,
        nombre : "Jorge Luis",
        apellido: "Borges",
        fechaDeNacimiento: "24/08/1899",
        libros: [
            {
                id: 1,
                titulo: "Ficciones",
                descripcion: "Se trata de uno de sus mas ...",
                anioPublicacion: 1944,
            },
            {
                id: 2,
                titulo: "El Aleph",
                descripcion: "Otra recopilacion de cuentos ...",
                anioPublicacion: 1949,
            }
        ]
    },
    {
        id: 2,
        nombre: "Gabriel",
        apellido: "Garcia Marquez",
        fechaDeNacimiento: "06/03/1927",
        libros: [
            {
                id: 1,
                titulo: "Los funerales de la Mamá Grande",
                descripcion: "Es una colección de ocho cuentos",
                anioPublicacion: 1962,
            },
            {
                id: 2,
                titulo: "Ojos de perro azul",
                descripcion: "es una antología que reúne los primeros cuentos del escritor",
                anioPublicacion: 1974,
            }
        ]
    },
    {
        id: 3,
        nombre: "Julio Florencio",
        apellido: "Cortázar",
        fechaDeNacimiento: "06/08/1914",
        libros: [
            {
                id: 1,
                titulo: "Bestirario",
                descripcion: "Es el título del primer libro de cuentos del escritor",
                anioPublicacion: 1951,
            },
            {
                id: 2,
                titulo: "Final del juego",
                descripcion: "Se le considera una de las obras más aclamadas e influyentes del autor",
                anioPublicacion: 1956,
            }
        ]
    }
]




module.exports = arrayAutores